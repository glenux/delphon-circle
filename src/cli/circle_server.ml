
open Core_kernel.Std
open Opium.Std

type person = {
  name: string;
  age: int;
}

let json_of_person { name ; age } =
  let open Ezjsonm in
  dict [ 
      "name", (string name) ; 
      "age", (int age) 
  ]

let print_param = put "/hello/:name" begin fun req ->
    `String ("Hello " ^ param req "name") 
    |> respond'
end

let print_person = 
  let pistache req =
      let person = {
         name = param req "name";
         age = Int.of_string @@ param req "age"
      } in
      `Json (person |> json_of_person |> Ezjsonm.wrap) |> respond'
  in
  get "/person/:name/:age" pistache

let _ =
  App.empty
  |> print_param
  |> print_person
  |> App.run_command

